return require('packer').startup(function(use)
    -- colorscheme and airline
    use 'vim-airline/vim-airline'
    use 'vim-airline/vim-airline-themes'
    use 'chriskempson/base16-vim'

    -- nvim tree stuff
    use 'kyazdani42/nvim-web-devicons'
    use 'kyazdani42/nvim-tree.lua'

    -- lsp plugins
    use 'neovim/nvim-lspconfig'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/nvim-cmp'
    use 'darrikonn/vim-gofmt'

    -- snippets
    use 'hrsh7th/cmp-vsnip'
    use 'hrsh7th/vim-vsnip'

    -- indent guides
    use 'lukas-reineke/indent-blankline.nvim'

    -- glsl syntax highlighting
    use 'tikhomirov/vim-glsl'

    -- nvim hop
    use 'phaazon/hop.nvim'

    -- syntax highlighting
    use 'LnL7/vim-nix'
    use 'ron-rs/ron.vim'
    use 'nickel-lang/vim-nickel'

    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
            ts_update()
		end,
    }
end)
