call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'cespare/vim-toml'
Plug 'rust-lang/rust.vim'
Plug 'morhetz/gruvbox'
Plug 'chriskempson/base16-vim'
Plug 'Raimondi/delimitMate'
Plug 'nvim-lua/plenary.nvim'

Plug 'jose-elias-alvarez/null-ls.nvim'
Plug 'jose-elias-alvarez/nvim-lsp-ts-utils'

Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'

"""language server and completions
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'darrikonn/vim-gofmt'

"""snippets
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

"""indent guides
Plug 'lukas-reineke/indent-blankline.nvim'
call plug#end()

let g:airline#extensions#coc#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = "base16_default"

set rnu
set nu
set fillchars+=vert:\ 
set tabstop=4
set expandtab
set hidden
set termguicolors
set completeopt-=preview
set omnifunc=syntaxcomplete#Complete
set nowrap
set completeopt=menu,menuone,noselect
set autoread

map <C-\> :NvimTreeToggle<CR>

"""au FileType rust nmap gd <Plug>(rust-def)
"""au FileType rust nmap gs <Plug>(rust-def-split)
"""au FileType rust nmap gx <Plug>(rust-def-vertical)
"""au FileType rust nmap <leader>gd <Plug>(rust-doc)

set t_Co=256
colorscheme base16-default-dark
highlight Normal ctermbg=NONE guibg=NONE
highlight nonText ctermbg=NONE

autocmd BufReadPost *.rs setlocal filetype=rust
autocmd Filetype css setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType scala setlocal ts=3 sts=3 sw=3 expandtab
autocmd FileType go setlocal ts=4 sts=4 sw=4 noexpandtab
autocmd BufWritePre go lua vim.lsp.buf.formatting()
augroup RestoreCursorShapeOnExit
    autocmd!
    autocmd VimLeave * set guicursor=a:ver25
augroup END

set title
set titlestring=nvim\ ::\ %t
exec "set titleold=".hostname()

nnoremap <C-n> :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>

lua require('config')
lua require('lsp')
lua require('tree')

autocmd BufWritePre *.go lua OrgImports(1000)
autocmd FileType go setlocal omnifunc=v:lua.vim.lsp.omnifunc
