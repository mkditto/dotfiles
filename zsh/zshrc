# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/mike/.zshrc'

alias ls="ls --color=auto"
alias ll="ls -l"
alias la="ls -la"

autoload -Uz compinit
compinit
# End of lines added by compinstall
ZSH_CONFIG_DIR=$HOME/.config/zsh

# import some stuff to make it feel more like fish
source $ZSH_CONFIG_DIR/git-prompt.zsh/git-prompt.zsh
source $ZSH_CONFIG_DIR/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH_CONFIG_DIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH_CONFIG_DIR/zsh-abbr/zsh-abbr.zsh
source $ZSH_CONFIG_DIR/zsh-window-title/zsh-window-title.zsh

zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

# zsh git prompt customization
ZSH_GIT_PROMPT_FORCE_BLANK=1
ZSH_GIT_PROMPT_SHOW_UPSTREAM="symbol"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[default]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX=")"
ZSH_THEME_GIT_PROMPT_SEPARATOR="|"
ZSH_THEME_GIT_PROMPT_DETACHED="%{$fg_no_bold[cyan]%}:"
ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_UPSTREAM_SYMBOL=""
ZSH_THEME_GIT_PROMPT_UPSTREAM_PREFIX="%{$fg[red]%}(%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_UPSTREAM_SUFFIX="%{$fg[red]%})"
ZSH_THEME_GIT_PROMPT_BEHIND="%{$fg_no_bold[cyan]%}↓"
ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg_no_bold[cyan]%}↑"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[red]%}✖"
ZSH_THEME_GIT_PROMPT_STAGED="%{$fg[green]%}●"
ZSH_THEME_GIT_PROMPT_UNSTAGED="%{$fg[red]%}✚"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[blue]%}…"
ZSH_THEME_GIT_PROMPT_STASHED="%{$fg[blue]%}⚑"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}✔"

# set prompts
PROMPT='%{$fg_bold[red]%}[%{$fg_bold[yellow]%}${USER}%{$fg_bold[green]%}@%{$fg[blue]%}%m %{$fg[magenta]%}%~%{$fg[red]%}]%{$fg[default]%}$ '
RPROMPT='$(gitprompt)'

# zsh syntax highlighting
typeset -A ZSH_HIGHLIGHT_STYLES

ZSH_HIGHLIGHT_STYLES[builtin]='fg=blue'
ZSH_HIGHLIGHT_STYLES[command]='fg=blue'
ZSH_HIGHLIGHT_STYLES[alias]='fg=blue'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=blue'
ZSH_HIGHLIGHT_STYLES[default]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=cyan'
ZSH_HIGHLIGHT_STYLES[path]='fg=cyan'

export QT_QPA_PLATFORMTHEME="qt5ct"
export VISUAL="nvim"
export MOZ_ENABLE_WAYLAND=1
export MANPAGER="sh -c 'col -bx | bat -l man'"
export POLYGON_KEY="JncA2_tG82oqnPqGQHyMj2BW_h2jCoQl"
export WINIT_UNIX_BACKEND="x11"

export GOPATH="$HOME/.local/share/go"
export CARGO_HOME="$HOME/.local/share/cargo"
export RUSTUP_HOME="$HOME/.local/share/rustup"
export CARGO_TARGET_DIR="$HOME/.cache/cargo"

export PATH="$PATH:/home/mike/.local/share/bin"
export PATH="$PATH:/Applications"
export PATH="$PATH:/home/mike/.local/bin"
export PATH="$PATH:$CARGO_HOME/bin"
export PATH="$PATH:/usr/local/bin:/usr/local/lib"
export PATH="$PATH:$HOME/.local/share/go/bin"

alias update='paru -Syu; flatpak update; rustup update'
alias batdiff='git diff --name-only --diff-filter=d | xargs bat --diff'
alias htop='btm -b'

# adding abbreviations is pretty slow, which kinda sucks. forking off setting them up
# stops it from noticably slowing down shell startup
((
    abbr -q n="nvim"
    abbr -q gcl="git clone"
    abbr -q gsu="git submodule update --recursive"
    abbr -q gsi="git submodule update --init --recursive"
    abbr -q gco="git checkout"
    abbr -q gpl="git pull"
    abbr -q gps="git push"
    abbr -q gpf="git push -f"
    abbr -q gad="git add"
    abbr -q gaa="git add ."
    abbr -q gcm="git commit -m"
    abbr -q gca="git commit --amend"
    abbr -q grb="git rebase"
    abbr -q gst="git status"
)&)
